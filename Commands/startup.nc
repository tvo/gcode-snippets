G91 ; incremental
G21 ; metric

; Resolution
M92 X100 Y100 Z400
; Max feedrate
M203 X22 Y22 Z5
; Max acceleration
M201 X100 Y200 Z150
; Set travel acceleration (might not be necessary)
M204 T3200

; Endstops (M120 enable, M121 disable)
;M120
M121

; Inactivity shutdown. 
M85 600
