; Disable steppers. M18 and M84 seem to be identical.
M18

M5 ; Spindle off (could work with laser too?)
M107 ; Fan off
M81 ; Power supply off
