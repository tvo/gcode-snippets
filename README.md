# gcode snippets

This is a simple repository which has snippets of various gcode files which I have found handy.

These snippets have been designed to work with https://gitlab.com/tvo/gcode-terminal.git so that that project can easily consume these gcode files.


## Getting started
```
git clone https://gitlab.com/tvo/gcode-snippets.git
```

## File structure

- [ ] [Commands] Each file here represents a specially named gcode file which can then be used as a "command". Ie home.nc, or startup.nc.- [ ] [Hooks] A cnc has various lifecycle operations. The files here are desiged to be run when that event happens.

## Naming convensions
Each gcode file should have the extension ".nc" - this seems to be a more standard approach than a ".gcode" extension.
The names should generally not repeat even between folders. This is to avoid confusion incase a script ever might include another script via an include (if that exists).

## Roadmap
String interpolation using a syntax like handlebars. A dynamic gcode syntax would be pretty cool.

## Contributing
Open to contributions here. I'm a hobbiest when it comes to cncs, I still have much to learn. If you have some useful commands feel free to create a pull request or create an issue so that this repository can grow and become a better knowledgebase.

## License
MIT
